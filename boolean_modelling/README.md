```markdown
## R Script Usage Guide

This guide explains how to use the R script designed to process configuration and boundary files, along with a parameter CSV file, and to perform subsequent data processing.

## Prerequisites

To run this script, you need to have R installed on your system along with the following R libraries:
- `data.table`: Provides an enhanced version of `data.frame` that is faster and more convenient.
- `optparse`: Facilitates the parsing of command line options.

You can install these libraries using the following R commands:
```R
install.packages("data.table")
install.packages("optparse")
```

## Script Files

Ensure the following scripts and files are in the same directory:
- The main R script (`workflowRun_name.R`)
- A Python script (`getting_probTables.py`) called within the R script.
- Another R script (`parsingTraj.R`) also invoked by the main script.

## Required Inputs

The script requires three command-line arguments to run:
- `-b, --bnd`: Path to the boundary file.
- `-c, --cfg`: Path to the configuration file.
- `-p, --param`: Path to the CSV file containing parameters.

Each of these files must be specified for the script to run correctly.

## Running the Script

To execute the script, use the following command format from your terminal:

```sh
./script_name.R --bnd /path/to/boundary/file --cfg /path/to/config/file --param /path/to/param/csv
```

Replace `/path/to/boundary/file`, `/path/to/config/file`, and `/path/to/param/csv` with the actual paths to your files.

## Output

The script processes the input files to update configuration settings based on the CSV parameters and generates restructured CSV files as output. The outputs are saved in a specified result directory.

- The updated configuration file will be saved with an `_updated.cfg` suffix.
- Processed CSV outputs are saved with a descriptive prefix in the result directory.

## Additional information

- Ensure all paths are correctly specified.
- The script checks for the existence of necessary directories and files before processing.
- Errors related to file paths or missing parameters will halt the execution and provide a relevant error message.
- To run the analysis in other tools like GINSIM and BoolNet you need to sanitize the model before using sanitize.R and qualmapper.R

```
