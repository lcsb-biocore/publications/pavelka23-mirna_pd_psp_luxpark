# Supporting code for "Converging Peripheral Blood MicroRNA Profiles in Idiopathic Parkinson’s Disease and Progressive Supranuclear Palsy"

This repository collects the code used in the analysis for publication "Converging Peripheral Blood MicroRNA Profiles in Idiopathic Parkinson’s Disease and Progressive Supranuclear Palsy".

The raw data files required for reproducing the manuscript results using this code are not publicly available due to clinical sensitivity. Please follow the access procedure at [doi:10.17881/aytm-r037](https://doi.org/10.17881/aytm-r037) to get the data.

Details:

- The file miRNAs.Rmd (written by Armin Rauschenberger) includes the R code for the differential expression analysis and predictive modelling. (This script requires the miRNA expression data and the clinical data from LuxPark and PPMI and returns the non-sensitive summary statistics for the other analyses.)

- The file “enriched_pathway_analysis.R” (written by Enrico Glaab) performs the enriched pathway analysis.

- The files in the folder “boolean_modelling” (written by Ahmed Hemedan) perform Boolean modelling.
